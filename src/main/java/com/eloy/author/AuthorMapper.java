package com.eloy.author;

import com.eloy.literature.LiteratureResponse;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public abstract class AuthorMapper {

    public abstract Author toEntity(AuthorResponse response);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Author> toEntityList(List<AuthorResponse> responseList);

    public abstract AuthorResponse toResponse(Author author);

    @Mapping(target = "id", source = "authorId")
    public abstract AuthorResponse toResponse(Author author, Long authorId);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<AuthorResponse> toResponseList(List<Author> authorList);
}
