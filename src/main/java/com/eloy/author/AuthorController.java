package com.eloy.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin("*")
@RequestMapping("/author")
@RestController
public class AuthorController {

    private final AuthorClientService authorClientService;

    public AuthorController(AuthorClientService authorClientService) {
        this.authorClientService = authorClientService;
    }

    @GetMapping(value = "/search/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<AuthorResponse>> findById(@PathVariable(value = "id") Long id) {
        return authorClientService.findById(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/new", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<AuthorResponse>> create(@RequestBody Author author) {
        return authorClientService.create(author).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PatchMapping(value = "/edit/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<AuthorResponse>> edit(@PathVariable(value = "id") Long id, @RequestBody Author author) {
        return authorClientService.update(id, author).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/id={id}")
    public Mono<ResponseEntity<String>> delete(@PathVariable(value = "id") Long id) {
        return authorClientService.delete(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
