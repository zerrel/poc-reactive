package com.eloy.author;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AuthorClientService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorClientService.class);

    private final WebClient webClient;

    private final AuthorMapper authorMapper;

    public AuthorClientService(WebClient.Builder builder, AuthorMapper authorMapper,
                               @Value("${global.base.url}") String baseUrl,
                               @Value("${global.mime.type}") String mimeType,
                               @Value("${global.user.agent}") String userAgent) {
        this.authorMapper = authorMapper;
        this.webClient = builder
                .baseUrl(baseUrl + "/author")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, mimeType)
                .defaultHeader(HttpHeaders.USER_AGENT, userAgent)
                .build();
    }

    public Mono<AuthorResponse> findById(Long id) {
        return webClient.get()
                .uri("/search/id=" + id)
                .retrieve()
                .bodyToMono(Author.class)
                .map(authorMapper::toResponse);
    }

    public Mono<AuthorResponse> create(Author author) {
        return webClient.post()
                .uri("/new")
                .body(BodyInserters.fromObject(authorMapper.toResponse(author)))
                .exchange()
                .flatMap(response -> response.bodyToMono(Author.class).map(authorMapper::toResponse));

    }

    public Mono<AuthorResponse> update(Long id, Author author) {
        return webClient.patch()
                .uri("/edit/id=" + id)
                .body(BodyInserters.fromObject(authorMapper.toResponse(author)))
                .exchange()
                .flatMap(response -> response.bodyToMono(Author.class).map(authorMapper::toResponse));
    }

    public Mono<String> delete(Long id) {
        return webClient.delete()
                .uri("/delete/" + id)
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class));
    }
}
