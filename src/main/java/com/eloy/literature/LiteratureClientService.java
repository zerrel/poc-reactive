package com.eloy.literature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
public class LiteratureClientService {

    private final WebClient webClient;

    private final AuthorLiteratureClientService authorLiteratureClientService;

    public LiteratureClientService(AuthorLiteratureClientService authorLiteratureClientService,
                                   @Value("${global.base.url}") String baseUrl,
                                   @Value("${global.mime.type}") String mimeType,
                                   @Value("${global.user.agent}") String userAgent) {
        this.authorLiteratureClientService = authorLiteratureClientService;
        this.webClient = WebClient.builder()
                .baseUrl(baseUrl + "/literature")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, mimeType)
                .defaultHeader(HttpHeaders.USER_AGENT, userAgent)
                .build();
    }

    public Mono<LiteratureResponse> findById(Long id) {
        return webClient
                .get()
                .uri("/search/id=" + id)
                .retrieve()
                .bodyToMono(Literature.class)
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);
    }

    public Mono<LiteratureResponse> findByPoi(String poi) {
        return webClient.get()
                .uri("/search/poi=" + poi)
                .retrieve()
                .bodyToMono(Literature.class)
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);
    }

    public Mono<LiteratureResponse> findByIsbn(String isbn) {
        return webClient.get()
                .uri("/search/isbn=" + isbn)
                .retrieve()
                .bodyToMono(Literature.class)
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);
    }

    public Mono<LiteratureResponse> findByUrl(String url) {
        return webClient.get()
                .uri("/search/url=" + url)
                .retrieve()
                .bodyToMono(Literature.class)
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);
    }

    public Mono<LiteratureResponse> create(Literature literature) {
        return webClient.post()
                .uri("/new")
                .body(BodyInserters.fromObject(literature))
                .exchange()
                .log()
                .flatMap(response -> response.bodyToMono(Literature.class))
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);

    }

    public Mono<LiteratureResponse> update(Long id, Literature literature) {
        System.out.println("SERVICE authorcount: " + literature.getAuthors().size());
        return webClient.patch()
                .uri("/edit/id=" + id)
                .body(BodyInserters.fromObject(literature))
                .exchange()
                .log()
                .flatMap(response -> response.bodyToMono(Literature.class))
                .flatMap(authorLiteratureClientService::getLiteratureWithAuthors);
    }

    public Mono<String> delete(Long id) {
        return webClient.delete()
                .uri("/delete/" + id)
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class));
    }
}
