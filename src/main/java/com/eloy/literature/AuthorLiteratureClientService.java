package com.eloy.literature;

import com.eloy.author.AuthorClientService;
import com.eloy.author.AuthorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorLiteratureClientService {

    private final LiteratureMapper literatureMapper;
    private final AuthorClientService authorClientService;

    public AuthorLiteratureClientService(LiteratureMapper literatureMapper, AuthorClientService authorClientService) {
        this.literatureMapper = literatureMapper;
        this.authorClientService = authorClientService;
    }

    Mono<LiteratureResponse> getLiteratureWithAuthors(Literature literature) {
        return Flux.fromIterable(literature.getAuthors())
                .flatMap(authorClientService::findById)
                .collectList()
                .map(e -> literatureMapper.toResponse(literature).authors(e));
    }
}
