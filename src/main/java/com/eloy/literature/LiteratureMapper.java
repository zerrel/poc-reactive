package com.eloy.literature;

import com.eloy.author.AuthorResponse;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class LiteratureMapper {

    abstract Literature toEntity(LiteratureResponse literature);

    Long toEntity(AuthorResponse author) {
        return author.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    abstract List<Literature> toEntityList(List<LiteratureResponse> responseList);

    abstract LiteratureResponse toResponse(Literature literature);

    @Mapping(target = "id", source = "literatureId")
    abstract LiteratureResponse toResponse(Literature literature, Long literatureId);

    AuthorResponse toResponse(Long authorId) {
        return new AuthorResponse().id(authorId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    abstract List<LiteratureResponse> toResponseList(List<Literature> literatureList);

}