package com.eloy.literature;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin("*")
@RequestMapping("/literature")
@RestController
public class LiteratureController {

    private final LiteratureClientService literatureClientService;

    public LiteratureController(LiteratureClientService literatureClientService) {
        this.literatureClientService = literatureClientService;
    }

    @GetMapping(value = "/search/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> findById(@PathVariable(value = "id") Long id) {
        return literatureClientService.findById(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/search/poi={poi}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> findByPoi(@PathVariable(value = "poi") String poi) {
        return literatureClientService.findByPoi(poi).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/search/isbn={isbn}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> findByIsbn(@PathVariable(value = "isbn") String isbn) {
        return literatureClientService.findByIsbn(isbn).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/search/url={url}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> findByUrl(@PathVariable(value = "url") String url) {
        return literatureClientService.findByUrl(url).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/new", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> create(@RequestBody Literature literature) {
        return literatureClientService.create(literature).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PatchMapping(value = "/edit/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<LiteratureResponse>> edit(@PathVariable(value = "id") Long id, @RequestBody Literature literature) {
        return literatureClientService.update(id, literature).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/id={id}")
    public Mono<ResponseEntity<String>> delete(@PathVariable(value = "id") Long id) {
        return literatureClientService.delete(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
