package com.eloy.project;

import com.eloy.user.UserResponse;
import com.eloy.util.ProjectState;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ProjectResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-27T14:53:15.219+01:00[Europe/Paris]")
public class ProjectResponse {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("state")
    private ProjectState state = null;

    @JsonProperty("subProjects")
    @Valid
    private List<ProjectResponse> subProjects = null;

    @JsonProperty("members")
    @Valid
    private List<UserResponse> members = null;

    public ProjectResponse id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProjectResponse name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectResponse state(ProjectState state) {
        this.state = state;
        return this;
    }

    /**
     * Get state
     *
     * @return state
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ProjectState getState() {
        return state;
    }

    public void setState(ProjectState state) {
        this.state = state;
    }

    public ProjectResponse subProjects(List<ProjectResponse> subProjects) {
        this.subProjects = subProjects;
        return this;
    }

    public ProjectResponse addSubProjectsItem(ProjectResponse subProjectsItem) {
        if (this.subProjects == null) {
            this.subProjects = new ArrayList<ProjectResponse>();
        }
        this.subProjects.add(subProjectsItem);
        return this;
    }

    /**
     * Get subProjects
     *
     * @return subProjects
     **/
    @ApiModelProperty(value = "")
    @Valid
    public List<ProjectResponse> getSubProjects() {
        return subProjects;
    }

    public void setSubProjects(List<ProjectResponse> subProjects) {
        this.subProjects = subProjects;
    }

    public ProjectResponse members(List<UserResponse> members) {
        this.members = members;
        return this;
    }

    public ProjectResponse addMembersItem(UserResponse membersItem) {
        if (this.members == null) {
            this.members = new ArrayList<UserResponse>();
        }
        this.members.add(membersItem);
        return this;
    }

    /**
     * Get members
     *
     * @return members
     **/
    @ApiModelProperty(value = "")
    @Valid
    public List<UserResponse> getMembers() {
        return members;
    }

    public void setMembers(List<UserResponse> members) {
        this.members = members;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectResponse project = (ProjectResponse) o;
        return Objects.equals(this.id, project.id) &&
                Objects.equals(this.name, project.name) &&
                Objects.equals(this.state, project.state) &&
                Objects.equals(this.subProjects, project.subProjects) &&
                Objects.equals(this.members, project.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, state, subProjects, members);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProjectResponse {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    state: ").append(toIndentedString(state)).append("\n");
        sb.append("    subProjects: ").append(toIndentedString(subProjects)).append("\n");
        sb.append("    members: ").append(toIndentedString(members)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
