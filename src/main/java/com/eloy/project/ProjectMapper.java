package com.eloy.project;

import com.eloy.user.UserResponse;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class ProjectMapper {

    @Mapping(target = "subProjects", ignore = true)
    abstract Project toEntity(ProjectResponse response);

    Long toEntity(UserResponse member) {
        return member.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    abstract List<Project> toEntityList(List<ProjectResponse> responseList);

    @Mapping(target = "subProjects", ignore = true)
    abstract ProjectResponse toResponse(Project Project);

    @Mapping(target = "id", source = "projectId")
    @Mapping(target = "subProjects", ignore = true)
    abstract ProjectResponse toResponse(Project project, Long projectId);

    UserResponse toResponse(Long member) {
        return new UserResponse().id(member);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    abstract List<ProjectResponse> toResponseList(List<Project> ProjectList);
}
