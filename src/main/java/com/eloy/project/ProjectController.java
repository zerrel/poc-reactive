package com.eloy.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin("*")
@RequestMapping("/project")
@RestController
public class ProjectController {

    private final ProjectClientService projectClientService;

    public ProjectController(ProjectClientService projectClientService) {
        this.projectClientService = projectClientService;
    }

    @GetMapping(value = "/search/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<ProjectResponse>> findById(@PathVariable(value = "id") Long id) {
        return projectClientService.findById(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/search/name={name}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<ProjectResponse>> findByName(@PathVariable(value = "name") String name) {
        return projectClientService.findByName(name).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/new", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<ProjectResponse>> create(@RequestBody Project project) {
        return projectClientService.create(project).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PatchMapping(value = "/edit/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<ProjectResponse>> edit(@PathVariable(value = "id") Long id, @RequestBody Project project) {
        return projectClientService.update(id, project).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/id={id}")
    public Mono<ResponseEntity<String>> delete(@PathVariable(value = "id") Long id) {
        return projectClientService.delete(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
