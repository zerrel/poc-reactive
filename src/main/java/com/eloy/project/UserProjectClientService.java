package com.eloy.project;

import com.eloy.user.UserClientService;
import com.eloy.user.UserResponse;
import com.eloy.user.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProjectClientService {

    private final Logger logger = LoggerFactory.getLogger(UserProjectClientService.class);

    private final UserClientService userClientService;

    private final ProjectMapper projectMapper;
    private final UserMapper userMapper;

    public UserProjectClientService(UserClientService userClientService, ProjectMapper projectMapper, UserMapper userMapper) {
        this.userClientService = userClientService;
        this.projectMapper = projectMapper;
        this.userMapper = userMapper;
    }

    Mono<ProjectResponse> getProjectWithUsers(Project project) {
        return Flux.fromIterable(project.getMembers())
                .flatMap(userClientService::findById)
                .collectList()
                .map(e -> projectMapper.toResponse(project).members(e));
    }
}
