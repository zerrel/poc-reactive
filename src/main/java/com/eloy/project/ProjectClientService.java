package com.eloy.project;

import com.eloy.util.ProjectState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProjectClientService {

    private static final Logger logger = LoggerFactory.getLogger(ProjectClientService.class);

    private final WebClient webClient;

    private final ProjectMapper projectMapper;

    private final UserProjectClientService userProjectClientService;

    public ProjectClientService(ProjectMapper projectMapper, UserProjectClientService userProjectClientService,
                                @Value("${global.base.url}") String baseUrl,
                                @Value("${global.mime.type}") String mimeType,
                                @Value("${global.user.agent}") String userAgent) {
        this.userProjectClientService = userProjectClientService;
        this.projectMapper = projectMapper;
        this.webClient = WebClient.builder()
                .baseUrl(baseUrl + "/project")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, mimeType)
                .defaultHeader(HttpHeaders.USER_AGENT, userAgent)
                .build();
    }

    public Mono<ProjectResponse> findById(Long id) {
        return webClient.get()
                .uri("/search/id=" + id)
                .retrieve()
                .bodyToMono(Project.class)
                .flatMap(userProjectClientService::getProjectWithUsers);
    }

    public Mono<ProjectResponse> findByName(String name) {
        return webClient.get()
                .uri("/search/name=" + name)
                .retrieve()
                .bodyToMono(Project.class)
                .flatMap(userProjectClientService::getProjectWithUsers);
    }

    public Mono<ProjectResponse> create(Project project) {
        return webClient.post()
                .uri("/new")
                .body(BodyInserters.fromObject(project))
                .exchange()
                .log()
                .flatMap(response -> response.bodyToMono(Project.class))
                .flatMap(userProjectClientService::getProjectWithUsers);
    }

    public Mono<ProjectResponse> update(Long id, Project project) {
        return webClient.patch()
                .uri("/edit/id=" + id)
                .body(BodyInserters.fromObject(project))
                .exchange()
                .flatMap(response -> response.bodyToMono(Project.class))
                .flatMap(userProjectClientService::getProjectWithUsers);
    }

    public Mono<String> delete(Long id) {
        return webClient.delete()
                .uri("/project/delete/" + id)
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class));
    }
}
