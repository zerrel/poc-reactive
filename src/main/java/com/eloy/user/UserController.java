package com.eloy.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin("*")
@RequestMapping("/user")
@RestController
public class UserController {

    private final UserClientService userClientService;

    public UserController(UserClientService userClientService) {
        this.userClientService = userClientService;
    }

    @GetMapping(value = "/search/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<UserResponse>> findById(@PathVariable(value = "id") Long id) {
        return userClientService.findById(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/search/username={username}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<UserResponse>> findByUsername(@PathVariable(value = "username") String username) {
        return userClientService.findByUsername(username).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/new", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<UserResponse>> create(@RequestBody User user) {
        return userClientService.create(user).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PatchMapping(value = "/edit/id={id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<ResponseEntity<UserResponse>> edit(@PathVariable(value = "id") Long id, @RequestBody User user) {
        return userClientService.update(id, user).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/id={id}")
    public Mono<ResponseEntity<String>> delete(@PathVariable(value = "id") Long id) {
        return userClientService.delete(id).map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
