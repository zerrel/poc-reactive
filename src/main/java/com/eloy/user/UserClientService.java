package com.eloy.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserClientService {

    private static final Logger logger = LoggerFactory.getLogger(UserClientService.class);

    private final WebClient webClient;

    private final UserMapper userMapper;

    public UserClientService(UserMapper userMapper,
                             @Value("${global.base.url}") String baseUrl,
                             @Value("${global.mime.type}") String mimeType,
                             @Value("${global.user.agent}") String userAgent) {
        this.userMapper = userMapper;
        this.webClient = WebClient.builder()
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, mimeType)
                .defaultHeader(HttpHeaders.USER_AGENT, userAgent)
                .build();
    }

    public Flux<UserResponse> findAll() {
        return webClient.get()
                .uri("/user/search/all")
                .exchange()
                .flatMapMany(clientResponse -> clientResponse.bodyToFlux(User.class).map(userMapper::toResponse));
    }

    public Mono<UserResponse> findById(Long id) {
        return webClient.get()
                .uri("/user/search/id=" + id)
                .retrieve()
                .bodyToMono(User.class).map(userMapper::toResponse);
    }

    public Mono<UserResponse> findByUsername(String username) {
        return webClient.get()
                .uri("/user/search/username=" + username)
                .retrieve()
                .bodyToMono(User.class).map(userMapper::toResponse);
    }

    public Mono<UserResponse> create(User user) {
        return webClient.post()
                .uri("/user/new")
                .body(BodyInserters.fromObject(userMapper.toResponse(user)))
                .exchange()
                .log()
                .flatMap(response -> response.bodyToMono(User.class).map(userMapper::toResponse));

    }

    public Mono<UserResponse> update(Long id, User user) {
        return webClient.patch()
                .uri("/user/edit/id=" + id)
                .body(BodyInserters.fromObject(userMapper.toResponse(user)))
                .exchange()
                .log()
                .flatMap(response -> response.bodyToMono(User.class).map(userMapper::toResponse));
    }

    public Mono<String> delete(Long id) {
        return webClient.delete()
                .uri("/user/delete/" + id)
                .exchange()
                .flatMap(response -> response.bodyToMono(String.class));
    }
}
