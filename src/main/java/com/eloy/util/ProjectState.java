package com.eloy.util;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ProjectState {
    IN_PREPARATION("In Preparation"),
    IN_PROGRESS("In Progress"),
    ON_HOLD("On Hold"),
    COMPLETED("Completed"),
    OTHER("Other");

    public String value;

    ProjectState(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }
}
